; Кнопка выключения скрипта
HotKeySet("{ESC}", "closeBot")

; Переключатель если мобы уже назлены
$angryMobs = true

While 1

   $attackBtn = PixelSearch(1087, 425, 1225, 440, 0xD61E11)
   If Not @error Then
	  MouseClick("left", $attackBtn[0]+25, $attackBtn[1])
	  Sleep(6000)
   EndIf

   If $angryMobs = true Then
	  MouseClick("left", 1080, 220, 2)
	  $angryMobs = false
   EndIf

   $combatBtn = PixelSearch(735, 470, 760, 488, 0xD2D22C)
   If Not @error Then
	  MouseClick("left", $combatBtn[0], $combatBtn[1])
	  Sleep(1000)
   EndIf

   $exitBtn = PixelSearch(778, 400, 783, 404, 0xEEDDAA)
   If Not @error Then
	  MouseClick("left", $exitBtn[0], $exitBtn[1])
	  $angryMobs = true
	  Sleep(2000)
   EndIf

WEnd

; Функция выключения скрипта
Func closeBot()
   Exit 0
EndFunc




