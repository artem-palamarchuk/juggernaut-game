$x = 335
$y = 240

HotKeySet("{ESC}", "closeBot")

While 1

   ; Кнопка для инициации атаки (нажать на меч)
   $attackBtn = PixelSearch(1087, 381, 1225, 395, 0xD61E11)
   If Not @error Then
	  Sleep(20000)
	  MouseClick("left", $attackBtn[0]+25, $attackBtn[1])
	  Sleep(10000)
   EndIf

   ; Healing
   $healBar = PixelSearch(104, 145, 118, 154, 0x561003)
   If Not @error Then
	  MouseClick("left", $x, $y)
	  $y = $y + 55
   EndIf

   ; Magic Punch
   $punchMagic = PixelSearch(765, 550, 810, 595, 0x3589D1)
   If Not @error Then
	  Sleep(5000)
	  MouseClick("left", $punchMagic[0], $punchMagic[1])
	  Sleep(2000)
   EndIf

   ; Проверка - загорелась левая или правая кнопка
   $combatLeftBtn = PixelSearch(440, 455, 925, 520, 0xFFFF44)
   If Not @error Then
	  MouseClick("left", $combatLeftBtn[0], $combatLeftBtn[1])
	  MouseMove(650,315)
	  Sleep(1000)
   EndIf

   ; Проверка - загорелась средняя кнопка
   $combatMiddleBtn = PixelSearch(440, 455, 925, 520, 0xFFFF2D)
   If Not @error Then
	  MouseClick("left", $combatMiddleBtn[0], $combatMiddleBtn[1])
	  MouseMove(650,315)
	  Sleep(1000)
   EndIf

   ; Выход из боя
   $exitBtn = PixelSearch(778, 400, 783, 404, 0xEEDDAA)
   If Not @error Then
	  MouseClick("left", $exitBtn[0], $exitBtn[1])
	  Sleep(1000)
   EndIf

WEnd


Func closeBot()
   Exit 0
EndFunc







