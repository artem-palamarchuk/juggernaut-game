$x = 335
$y = 240

HotKeySet("{ESC}", "closeBot")

; Переключатель если мобы уже назлены
$angryMobs = true

While 1

   ; Кнопка для инициации атаки (нажать на меч)
   $attackBtn = PixelSearch(1087, 338, 1225, 350, 0xD61E11)
   If Not @error Then
	  MouseClick("left", $attackBtn[0]+25, $attackBtn[1])
	  Sleep(6000)
   EndIf

   If $angryMobs = true Then
	  MouseClick("left", 1080, 220, 3)
	  $angryMobs = false
   EndIf

   ; Healing
   $healBar = PixelSearch(104, 145, 118, 154, 0x561003)
   If Not @error Then
	  MouseClick("left", $x, $y)
	  $y = $y + 55
   EndIf

   ; Magic Punch #231F24 - синяя перчатка #FCFCF3
   $punchMagic = PixelSearch(830, 550, 880, 600, 0xD3D7E0)
   If Not @error Then
	  Sleep(8000)
	  MouseClick("left", $punchMagic[0], $punchMagic[1])
	  Sleep(2000)
   EndIf

   ; Проверка - загорелась левая или правая кнопка
   $combatLeftBtn = PixelSearch(440, 455, 925, 520, 0xFFFF44)
   If Not @error Then
	  MouseClick("left", $combatLeftBtn[0], $combatLeftBtn[1])
	  MouseMove(700,500,10)
	  Sleep(1000)
   EndIf

   ; Проверка - загорелась средняя кнопка
   $combatMiddleBtn = PixelSearch(440, 455, 925, 520, 0xFFFF2D)
   If Not @error Then
	  MouseClick("left", $combatMiddleBtn[0], $combatMiddleBtn[1])
	  MouseMove(700,500,10)
	  Sleep(1000)
   EndIf

   ; Выход из боя
   $exitBtn = PixelSearch(778, 400, 783, 404, 0xEEDDAA)
   If Not @error Then
	  MouseClick("left", $exitBtn[0], $exitBtn[1])
	   $angryMobs = true
	  Sleep(4000)
   EndIf

WEnd


Func closeBot()
   Exit 0
EndFunc







